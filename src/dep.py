def sum(a, b):
    """Compute and return the sum of two numbers.

    Usage examples:
    >>> sum(4, 2)
    6
    >>> sum(7, 9)
    16
    """
    return int(a) + int(b)
